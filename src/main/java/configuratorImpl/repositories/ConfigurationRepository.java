package configuratorImpl.repositories;

import configuratorImpl.models.Configuration;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface ConfigurationRepository extends configurator.repositories.ConfigurationRepository, PagingAndSortingRepository<Configuration, Long> {

}
