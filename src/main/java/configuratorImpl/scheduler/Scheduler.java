package configuratorImpl.scheduler;

import configurator.services.ConfigurationReader;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class Scheduler implements InitializingBean {

    @Autowired
    private ConfigurationReader reader;

    @Autowired
    ThreadPoolTaskScheduler threadPoolTaskScheduler;

    @Override
    public void afterPropertiesSet() throws Exception {
        threadPoolTaskScheduler.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                System.out.println("Refreshing");
                reader.refresh();
            }
        }, reader.getRefreshTime());
    }
}
