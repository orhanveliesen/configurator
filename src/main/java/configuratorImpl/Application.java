package configuratorImpl;


import configurator.services.ConfigurationReader;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;


@SpringBootApplication
@EnableScheduling
@EntityScan(basePackages = "configuratorImpl.models")
public class Application {

        public static void main(String[] args){
            SpringApplication.run(Application.class, args);
        }


        @Bean
        public ConfigurationReader configurationReader(ConfiguratorConfig config, configurator.repositories.ConfigurationRepository configurationRepository){
            ConfigurationReader reader =  new ConfigurationReader(config.getApplicationName(), configurationRepository, config.getRefreshTime());

            return reader;
        }

        @Bean
        public ThreadPoolTaskScheduler threadPoolTaskScheduler(){
            ThreadPoolTaskScheduler threadPoolTaskScheduler
                    = new ThreadPoolTaskScheduler();
            threadPoolTaskScheduler.setPoolSize(3);
            threadPoolTaskScheduler.setThreadNamePrefix(
                    "ThreadPoolTaskScheduler");
            return threadPoolTaskScheduler;
        }



}
